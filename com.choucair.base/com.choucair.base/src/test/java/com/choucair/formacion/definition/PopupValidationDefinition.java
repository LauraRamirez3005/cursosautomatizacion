package com.choucair.formacion.definition;

import com.choucair.formacion.steps.PopupValidationSteps;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PopupValidationDefinition {
	
	@Steps
	PopupValidationSteps popupValidationSteps;

	@Given("^Autentico en colorlib con usuario \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void autentico_en_colorlib_con_usuario_y_clave(String strUsuario, String strPass) {
		popupValidationSteps.login_colorlib(strUsuario, strPass);
	}

	@Given("^Ingreso a la funcionalidad Forms Validation$")
	public void ingreso_a_la_funcionalidad_Forms_Validation() {
		popupValidationSteps.ingresar_forms_validation();
	}

	@When("^Diligencio Formulario Popup Validation$")
	public void diligencio_Formulario_Popup_Validation() {

	}

	@Then("^Verifico ingreso exitoso$")
	public void verifico_ingreso_exitoso() {

	}



}
